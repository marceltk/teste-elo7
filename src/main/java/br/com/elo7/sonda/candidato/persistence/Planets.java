package br.com.elo7.sonda.candidato.persistence;

import br.com.elo7.sonda.candidato.model.Planet;

import java.util.Optional;

public interface Planets {

    void save(Planet planet);

    Optional<Planet> findById(int id);

}
