package br.com.elo7.sonda.candidato.service;

import br.com.elo7.sonda.candidato.dto.InputDTO;
import br.com.elo7.sonda.candidato.dto.ProbeDTO;
import br.com.elo7.sonda.candidato.model.Command;
import br.com.elo7.sonda.candidato.model.Direction;
import br.com.elo7.sonda.candidato.model.Planet;
import br.com.elo7.sonda.candidato.model.Probe;
import br.com.elo7.sonda.candidato.persistence.Planets;
import br.com.elo7.sonda.candidato.persistence.Probes;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProbeService {

    @Autowired
    private Planets planets;
    @Autowired
    private Probes probes;

    public List<Probe> landProbes(InputDTO input) {
        Planet planet = concertPlanet(input);
        planets.save(planet);

        List<Probe> convertedProbes = convertAndMoveProbes(input, planet);
        convertedProbes.forEach(probe -> probes.save(probe));

        return convertedProbes;
    }

    @VisibleForTesting
    void applyCommandToProbe(Probe probe, char command) {
		switch (command) {
			case Command.R -> turnProbeRight(probe);
			case Command.L -> turnProbeLeft(probe);
			case Command.M -> moveProbeForward(probe);
		}
    }

    private void moveProbeForward(Probe probe) {
        int newX = probe.getX();
        int newY = probe.getY();

		switch (probe.getDirection()) {
			case Direction.N -> newY++;
			case Direction.W -> newX--;
			case Direction.S -> newY--;
			case Direction.E -> newX++;
		}

        probe.setX(newX);
        probe.setY(newY);
    }

    private void turnProbeLeft(Probe probe) {
        char newDirection = switch (probe.getDirection()) {
			case Direction.N -> Direction.W;
			case Direction.W -> Direction.S;
			case Direction.S -> Direction.E;
			case Direction.E -> Direction.N;

			default -> Direction.N;
		};
		probe.setDirection(newDirection);
    }

    private void turnProbeRight(Probe probe) {
        char newDirection = switch (probe.getDirection()) {
			case Direction.N -> Direction.E;
			case Direction.E -> Direction.S;
			case Direction.S -> Direction.W;
			case Direction.W -> Direction.N;
			default -> Direction.N;
		};
		System.out.println(newDirection);
        probe.setDirection(newDirection);

    }

    private List<Probe> convertAndMoveProbes(InputDTO input, Planet planet) {
        return input.getProbes()
                .stream().map(probeDto -> {
                    Probe probe = convertProbe(probeDto, planet);
                    moveProbeWithAllCommands(probe, probeDto);
                    return probe;
                }).collect(Collectors.toList());
    }

    private void moveProbeWithAllCommands(Probe probe, ProbeDTO probeDTO) {
        for (char command : probeDTO.getCommands().toCharArray()) {
            applyCommandToProbe(probe, command);
        }
    }

    private Probe convertProbe(ProbeDTO probeDto, Planet planet) {
        Probe probe = new Probe();
        probe.setPlanet(planet);
        probe.setX(probeDto.getX());
        probe.setY(probeDto.getY());
        probe.setDirection(probeDto.getDirection());
        return probe;
    }

    private Planet concertPlanet(InputDTO input) {
        Planet planet = new Planet();
        planet.setHeight(input.getHeight());
        planet.setWidth(input.getWidth());
        return planet;
    }
}
