package br.com.elo7.sonda.candidato.persistence;

import br.com.elo7.sonda.candidato.model.Probe;

import java.util.Optional;

public interface Probes {

    void save(Probe probe);

    Optional<Probe> findById(int id);

}
